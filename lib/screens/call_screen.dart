import 'package:flutter/material.dart';

class CallScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: fakeCallData.length,
      itemBuilder: (context, index) {
        final call = fakeCallData[index];
        return ListTile(
          leading: CircleAvatar(
            backgroundImage: NetworkImage(call.avatarUrl),
          ),
          title: Text(call.name),
          subtitle: Row(
            children: [
              Icon(call.isIncoming ? Icons.call_received : Icons.call_made),
              Text(call.time),
            ],
          ),
          trailing: Icon(Icons.call),
          onTap: () {
            // Aramayı başlatmak için bir işlev ekleyebilirsiniz.
          },
        );
      },
    );
  }
}

class Call {
  final String name;
  final String time;
  final String avatarUrl;
  final bool isIncoming;

  Call({
    required this.name,
    required this.time,
    required this.avatarUrl,
    required this.isIncoming,
  });
}

// Sahte arama verileri
List<Call> fakeCallData = [
  Call(
    name: "Mustafa",
    time: "Bugün, 10:30",
    avatarUrl: "https://photos.google.com/photo/AF1QipNCaD5FDEExMAo1g1urZ57_TkK5gqJEG6zToX_u",
    isIncoming: true,
  ),
  Call(
    name: "Cavid Ureklerde Osmen",
    time: "Dün, 14:20",
    avatarUrl: "https://photos.google.com/photo/AF1QipNCaD5FDEExMAo1g1urZ57_TkK5gqJEG6zToX_u",
    isIncoming: false,
  ),
  // Diğer aramalar...
];
