import 'package:flutter/material.dart';

class StatusScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: fakeStatusData.length,
      itemBuilder: (context, index) {
        final status = fakeStatusData[index];
        return ListTile(
          leading: CircleAvatar(
            backgroundImage: NetworkImage(status.avatarUrl),
          ),
          title: Text(
            status.name,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          subtitle: Text(status.time),
          onTap: () {
            // Durum detayına gitmek için bir işlev ekleyebilirsiniz.
          },
        );
      },
    );
  }
}

class Status {
  final String name;
  final String time;
  final String avatarUrl;

  Status({
    required this.name,
    required this.time,
    required this.avatarUrl,
  });
}

// Sahte durum verileri
List<Status> fakeStatusData = [
  Status(
    name: "Cavid",
    time: "Bugün, 10:30",
    avatarUrl: "https://photos.google.com/photo/AF1QipNCaD5FDEExMAo1g1urZ57_TkK5gqJEG6zToX_u",
  ),
  Status(
    name: "Mustafa",
    time: "Dün, 14:20",
    avatarUrl: "https://photos.google.com/photo/AF1QipNCaD5FDEExMAo1g1urZ57_TkK5gqJEG6zToX_u",
  ),
  // Diğer durumlar...
];
