import 'package:flutter/material.dart';
import 'package:mini_whatsapp/home.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
   const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Whatsapp Demo',
      theme: ThemeData(
        primaryColor: const Color(0xff075E54),
        hintColor: const Color(0xff25D366)
      ),
      home: const Home(),
    );
  }
}