class Chat {
  String name;
  String message;
  String time;
  String avatarUrl;

  Chat({ 
    required this.name,
    required this.message,
    required this.time,
    required this.avatarUrl});

}

List<Chat> fakeData=[
    Chat(
      name: "Mustafa", 
      message: "Osmen necesen", 
      time:"20:21", 
      avatarUrl:"https://photos.google.com/photo/AF1QipNCaD5FDEExMAo1g1urZ57_TkK5gqJEG6zToX_u"),
       
       Chat(
      name: "Cavid", 
      message: "Mans", 
      time:"20:21", 
      avatarUrl:"https://photos.google.com/photo/AF1QipPY5UBoYs2nW9ubxttCsax980yrcxZaImnzg5pL"),

      Chat(
      name: "ALiabbasovicoglu", 
      message: "Good evening aue", 
      time:"20:21", 
      avatarUrl:"https://photos.google.com/photo/AF1QipPMArm8fipYnfv7liDsXb2j_Q9idxcJOnf_ZKSI"),

  ];